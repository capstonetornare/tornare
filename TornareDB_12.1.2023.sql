-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 01, 2023 at 04:49 AM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tornare`
--

-- --------------------------------------------------------

--
-- Table structure for table `alumni`
--

CREATE TABLE `alumni` (
  `alumni_count` int(255) NOT NULL,
  `alumni_id` varchar(20) NOT NULL COMMENT 'Unique ID',
  `alumni_name` varchar(255) NOT NULL,
  `alumni_prgrm` enum('BSIT','CHM','','') NOT NULL,
  `alumni_year&lvl` varchar(255) NOT NULL,
  `alumni_photoname` varchar(255) NOT NULL COMMENT '"alumni_photoname".jpg'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `alumni`
--

INSERT INTO `alumni` (`alumni_count`, `alumni_id`, `alumni_name`, `alumni_prgrm`, `alumni_year&lvl`, `alumni_photoname`) VALUES
(1, '20120022', 'Shan Kyle S. Pepico', 'BSIT', '2023', 'ShanKyleSPepico'),
(2, '20161800', 'Vrod Rick D. Sanchez', 'BSIT', '2023', 'VrodRickDSanchez'),
(3, '20200550', 'Jess Michael Belen', 'BSIT', '2023', 'JessMichaelBelen'),
(4, '20042219', 'John Sirach C. Paladar', 'BSIT', '2023', 'JohnSirachCPaladar'),
(5, '20190143', 'Jake Marben A. Bantoto', '', '2024', 'JakeMarbenABantoto'),
(6, '20200273', 'Allen F. Pagnanawon', 'BSIT', '2024', 'AllenFPagnanawon');

-- --------------------------------------------------------

--
-- Table structure for table `buildings`
--

CREATE TABLE `buildings` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(500) NOT NULL,
  `map_active` varchar(5000) NOT NULL,
  `map_style` varchar(5000) NOT NULL,
  `loc_pos` varchar(255) NOT NULL COMMENT 'Fast Travel Coordinates',
  `status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `buildings`
--

INSERT INTO `buildings` (`id`, `name`, `description`, `map_active`, `map_style`, `loc_pos`, `status`) VALUES
(1, 'College of Business Administration', 'College of Business Administration content here', 'M5396 4055 c-11 -8 -31 -15 -45 -15 -14 0 -43 -6 -66 -14 -22 -8 -67\n        -22 -100 -31 -33 -9 -73 -21 -90 -27 -16 -6 -43 -12 -60 -15 -16 -3 -44 -11\n        -62 -19 -17 -8 -41 -14 -53 -14 -12 0 -31 -5 -43 -12 -12 -6 -40 -15 -62 -19\n        -22 -4 -55 -12 -72 -18 -18 -6 -48 -16 -65 -22 -18 -6 -51 -14 -73 -18 -22 -4\n        -50 -13 -62 -19 -12 -7 -33 -12 -46 -12 -14 0 -38 -7 -53 -15 -16 -8 -40 -15\n        -54 -15 -14 -1 -36 -7 -50 -15 -14 -8 -40 -14 -57 -15 -42 0 -43 -22 -2 -37\n        30 -12 31 -12 24 -68 -7 -54 -6 -58 25 -90 33 -34 33 -35 20 -74 -15 -45 -10\n        -55 42 -93 26 -18 34 -19 72 -8 27 7 131 12 279 13 l236 2 63 -62 c35 -35 70\n        -63 78 -63 8 0 35 7 60 15 25 8 58 17 73 20 17 3 31 15 37 30 6 17 17 25 32\n        25 21 0 68 13 153 42 17 6 48 14 70 18 22 5 57 15 78 22 l37 13 0 216 0 217\n        30 11 c17 5 30 13 30 16 0 10 -90 128 -112 148 -27 23 -183 24 -212 2z', 'fill:#fcee21', '', NULL),
(2, 'College of Architecture and Fine Arts', 'College of Architecture and Fine Arts content here\r\n', 'M5950 3619 c-36 -17 -78 -36 -95 -44 -16 -7 -40 -16 -52 -20 -28 -8\r\n        -30 -40 -5 -54 10 -5 24 -29 31 -53 7 -24 16 -50 20 -58 4 -8 101 -62 215\r\n        -120 l207 -104 32 17 c18 9 73 35 122 57 50 23 112 52 138 65 27 14 55 25 63\r\n        25 8 0 14 2 14 5 0 12 -36 45 -48 45 -12 0 -48 16 -92 41 -8 5 -22 12 -30 15\r\n        -8 3 -22 10 -30 15 -8 5 -21 11 -27 14 -7 3 -19 7 -25 10 -7 3 -21 9 -30 15\r\n        -10 5 -26 15 -35 20 -10 6 -25 13 -33 16 -8 3 -69 32 -135 65 -67 32 -125 59\r\n        -130 58 -6 0 -39 -14 -75 -30z', 'fill:#f923f9', '', NULL),
(3, 'College of Computer Studies', 'College of Computer Studies content here ', 'M3970 3485 c-14 -8 -56 -28 -95 -45 -38 -18 -82 -38 -97 -46 -14 -8\r\n        -35 -14 -47 -14 -11 0 -46 -7 -78 -15 -31 -8 -87 -15 -123 -15 -36 0 -100 -7\r\n        -143 -15 -43 -8 -109 -15 -145 -15 -37 -1 -96 -7 -132 -15 -36 -8 -94 -14\r\n        -130 -15 -36 0 -100 -7 -143 -15 -43 -8 -109 -15 -145 -15 -37 -1 -96 -7 -132\r\n        -15 -36 -8 -100 -14 -143 -15 -89 0 -101 -11 -75 -64 27 -54 36 -89 41 -159 3\r\n        -39 11 -75 19 -85 8 -9 18 -25 21 -34 5 -15 19 -18 79 -18 40 1 100 7 133 15\r\n        72 18 216 20 232 4 19 -19 114 -49 156 -49 21 0 51 -6 65 -13 15 -8 45 -22 67\r\n        -31 22 -10 53 -24 68 -32 16 -8 32 -14 37 -14 5 0 38 -13 73 -30 36 -16 74\r\n        -30 84 -30 11 0 28 -7 39 -15 20 -15 90 -18 134 -6 28 8 38 47 20 81 -17 31 0\r\n        52 108 133 168 125 203 144 307 164 17 3 35 9 40 13 6 4 44 25 85 47 41 22\r\n        102 55 135 73 33 18 84 45 113 60 30 15 61 38 69 50 16 23 16 23 -21 16 -35\r\n        -7 -43 -3 -109 47 -40 29 -80 56 -89 60 -29 10 -20 35 20 56 12 7 20 16 17 22\r\n        -10 15 -285 10 -315 -6z', 'fill:#8a8a8a', '', NULL),
(4, 'Sophia Solaire Sinco Hall', 'Sophia Solaire Sinco Hall content here ', 'M4520 3264 c-41 -38 -102 -80 -165 -114 -55 -29 -125 -67 -155 -83\r\n        -90 -50 -149 -77 -165 -77 -42 0 -135 -45 -218 -107 -51 -37 -103 -75 -117\r\n        -84 -21 -14 -23 -22 -21 -74 1 -32 9 -68 17 -79 8 -11 14 -43 14 -71 0 -45 -4\r\n        -54 -30 -73 -16 -12 -40 -22 -53 -22 -17 0 -26 -7 -30 -22 -8 -28 -1 -38 26\r\n        -38 12 -1 33 -7 47 -15 14 -8 37 -14 51 -15 14 0 34 -7 45 -15 10 -8 29 -15\r\n        41 -15 12 0 24 -4 28 -9 3 -5 20 -13 37 -17 18 -3 39 -10 48 -15 8 -4 38 -18\r\n        65 -30 28 -12 63 -28 78 -36 25 -13 31 -13 52 1 29 19 56 20 74 4 10 -11 16\r\n        -8 33 15 15 21 28 27 58 27 21 0 43 6 50 15 7 9 14 55 16 112 7 180 6 184 -23\r\n        215 -14 14 -23 30 -20 35 3 5 25 19 49 31 24 13 70 39 103 58 33 19 70 39 82\r\n        44 12 5 41 21 64 36 l43 27 48 -18 c39 -14 65 -16 137 -10 82 6 91 5 110 -14\r\n        12 -12 21 -32 21 -45 0 -26 30 -51 76 -62 15 -4 39 -13 53 -20 36 -19 152 -18\r\n        177 1 10 8 26 15 35 15 9 0 35 14 57 30 23 17 45 30 49 30 5 0 21 11 37 23\r\n        l29 24 -35 21 c-20 12 -42 22 -50 22 -17 0 -108 83 -108 98 0 6 7 15 16 20 23\r\n        13 11 28 -26 36 -23 4 -35 15 -45 40 -8 20 -25 41 -37 47 -13 7 -69 57 -126\r\n        112 l-102 101 -188 1 -187 0 -65 -61z', 'fill:#5c1621', '', NULL),
(5, 'College of Criminal and Justice', 'College of Criminal and Justice content here ', 'M5612 3297 c-35 -12 -81 -37 -103 -56 -22 -19 -50 -40 -62 -46 -29\r\n        -14 -29 -49 -1 -53 12 -2 30 -12 40 -23 16 -18 19 -39 19 -155 0 -85 5 -138\r\n        12 -145 11 -11 14 -10 148 51 33 16 83 38 110 50 28 12 64 28 80 35 24 11 51\r\n        12 120 5 50 -5 105 -13 123 -19 47 -14 256 -157 270 -184 7 -12 13 -71 14\r\n        -132 l3 -110 50 1 c62 0 132 30 139 58 3 12 22 29 43 40 21 10 45 22 53 27 8\r\n        5 22 12 30 15 8 3 27 12 42 20 14 8 34 14 45 14 10 1 29 10 42 21 18 15 21 23\r\n        13 31 -7 7 -12 22 -12 34 0 12 -7 27 -15 34 -22 18 -21 182 1 204 8 9 20 16\r\n        25 16 24 0 4 32 -38 63 -68 49 -90 59 -158 74 -33 7 -84 18 -113 25 -56 13\r\n        -56 13 -215 -68 -33 -16 -60 -18 -85 -4 -9 5 -39 19 -67 30 -54 23 -59 25\r\n        -155 68 -36 16 -87 39 -115 51 -27 12 -62 28 -77 36 -37 21 -132 17 -206 -8z', 'fill:#524337', '', NULL),
(6, 'College of Education', 'College of Education content here ', 'M1698 3283 c-16 -2 -28 -8 -28 -14 0 -5 -18 -12 -40 -15 -22 -4 -45\r\n        -13 -51 -20 -6 -8 -23 -14 -37 -14 -14 0 -34 -7 -44 -17 -15 -13 -23 -14 -43\r\n        -5 -26 12 -30 11 -112 -24 -17 -8 -38 -14 -46 -14 -9 0 -17 -3 -19 -7 -3 -9\r\n        -108 -53 -124 -53 -7 0 -17 -6 -24 -14 -6 -8 -23 -16 -38 -18 -17 -2 -35 -15\r\n        -50 -38 -13 -20 -37 -40 -55 -45 -18 -6 -45 -17 -62 -24 -16 -8 -46 -20 -65\r\n        -29 -19 -8 -47 -21 -62 -29 -14 -7 -35 -13 -46 -13 -11 0 -25 -7 -32 -15 -7\r\n        -8 -24 -15 -37 -15 -31 0 -26 -20 10 -38 17 -8 27 -21 27 -36 0 -17 10 -26 43\r\n        -40 23 -9 49 -21 57 -27 8 -5 27 -14 43 -20 38 -15 34 -33 -13 -54 -42 -19\r\n        -67 -14 -111 22 -14 12 -19 10 -33 -11 -10 -16 -30 -28 -52 -32 -23 -4 -38\r\n        -13 -41 -25 -4 -15 -129 -99 -148 -99 -3 0 -16 -9 -27 -20 -17 -16 -19 -23\r\n        -10 -32 7 -7 12 -19 12 -28 0 -17 33 -50 50 -50 6 0 26 -9 46 -19 22 -12 43\r\n        -17 53 -12 74 33 90 42 96 51 3 5 15 10 25 10 11 0 24 9 30 20 14 26 104 70\r\n        143 70 21 0 36 7 45 20 16 27 93 63 106 50 5 -5 22 -10 36 -10 14 0 35 -7 46\r\n        -15 10 -8 28 -15 39 -15 11 0 32 -7 47 -14 47 -25 113 -30 149 -12 24 12 35\r\n        13 45 5 14 -11 43 0 207 82 31 16 63 29 71 29 7 0 19 7 26 15 7 8 20 15 30 15\r\n        10 0 23 7 30 15 7 8 20 15 30 15 10 0 23 7 30 15 7 8 23 15 36 15 13 0 24 4\r\n        24 9 0 5 10 13 23 16 12 4 37 13 55 21 l32 15 0 127 c-1 84 -6 140 -15 167\r\n        -20 54 -19 73 5 87 28 18 25 24 -19 41 -22 9 -42 21 -46 27 -3 5 -17 10 -30\r\n        10 -13 0 -26 4 -30 9 -6 12 -110 62 -122 60 -4 -1 -20 -4 -35 -6z', 'fill:#002fba', '', NULL),
(7, 'Administration Room', 'Administration Room content here ', 'M6597 3269 c-43 -23 -41 -34 5 -42 126 -21 142 -28 245 -105 95 -71\r\n        122 -102 90 -102 -7 0 -29 -11 -50 -25 -33 -23 -37 -30 -37 -69 0 -32 6 -48\r\n        23 -64 27 -25 55 -28 63 -7 4 8 14 15 24 15 14 0 20 -9 22 -32 3 -32 9 -36\r\n        178 -119 96 -48 195 -96 220 -108 25 -12 52 -26 60 -30 51 -29 68 -33 89 -22\r\n        13 7 21 21 21 36 0 16 6 25 15 25 8 0 15 -9 15 -19 0 -26 15 -34 65 -34 37 0\r\n        90 23 107 47 3 5 4 24 1 42 -7 41 17 81 52 90 25 6 36 34 14 34 -11 0 -103 42\r\n        -324 150 -66 32 -246 120 -400 195 -154 75 -292 143 -307 151 -39 21 -145 17\r\n        -191 -7z', 'fill:#5c1621', '', NULL),
(8, 'College of Arts and Science', 'College of Arts and Science content here ', 'M5860 2911 c-15 -9 -47 -23 -165 -76 -64 -28 -65 -28 -220 -100 -143\r\n        -66 -158 -72 -177 -79 -10 -3 -18 -10 -18 -15 0 -10 61 -41 83 -41 9 0 18 -4\r\n        22 -9 3 -5 19 -12 36 -16 17 -4 42 -13 57 -21 15 -8 34 -14 43 -14 8 0 19 -4\r\n        25 -8 9 -8 37 -19 247 -98 85 -32 107 -44 107 -58 0 -7 -11 -17 -24 -20 -14\r\n        -3 -28 -15 -31 -26 -6 -18 -78 -65 -227 -148 -56 -31 -120 -42 -133 -23 -3 5\r\n        -19 12 -36 16 -17 4 -42 13 -57 21 -15 8 -32 14 -38 14 -7 0 -25 7 -40 15 -16\r\n        8 -36 15 -44 15 -9 0 -20 4 -25 8 -6 5 -39 19 -75 31 -36 13 -74 27 -85 32\r\n        -11 4 -63 24 -115 44 -52 20 -112 43 -132 51 -21 8 -56 21 -78 29 -22 8 -59\r\n        22 -82 31 -24 9 -60 22 -80 30 -82 30 -124 47 -155 60 -23 10 -34 10 -39 2 -9\r\n        -15 -9 -257 1 -286 3 -13 16 -27 28 -33 12 -5 29 -13 37 -18 38 -22 93 -41\r\n        120 -41 30 0 100 -28 111 -44 10 -13 79 -38 124 -43 22 -3 49 -12 60 -19 11\r\n        -8 27 -14 36 -14 9 0 22 -7 29 -15 7 -8 20 -15 29 -15 9 0 29 -7 45 -15 15 -8\r\n        46 -15 68 -15 37 0 71 -17 124 -63 21 -18 158 -57 201 -57 12 0 26 8 32 18 9\r\n        16 12 16 38 2 15 -9 34 -29 42 -45 8 -17 19 -33 25 -38 20 -14 66 2 119 42 28\r\n        22 64 44 80 50 21 8 27 17 27 41 0 18 -6 33 -15 36 -21 8 -21 74 0 74 26 0 29\r\n        30 6 51 -27 25 -26 30 4 63 14 15 25 33 25 41 0 13 255 145 280 145 17 0 227\r\n        110 241 126 16 20 13 242 -4 263 -10 12 -116 87 -165 116 -10 6 -20 14 -24 19\r\n        -13 19 -243 42 -268 27z', 'fill:#dbdbdb', '', NULL),
(9, 'DAFA', 'DAFA content here ', 'M4575 2765 c-44 -24 -87 -49 -95 -55 -8 -6 -27 -15 -41 -21 -25 -9\r\n        -26 -12 -15 -41 6 -17 20 -34 31 -38 37 -14 105 -40 145 -55 76 -28 125 -48\r\n        157 -61 17 -8 38 -14 46 -14 9 0 18 -4 22 -9 3 -5 19 -12 36 -16 17 -4 42 -13\r\n        57 -21 15 -8 35 -14 44 -14 9 0 19 -4 23 -9 3 -5 19 -12 36 -16 17 -4 42 -13\r\n        57 -21 15 -8 32 -14 38 -14 7 0 25 -7 40 -15 16 -8 36 -15 44 -15 8 0 24 -7\r\n        34 -15 11 -8 30 -15 42 -15 13 0 25 -4 28 -9 3 -5 38 -19 78 -32 40 -13 82\r\n        -29 93 -36 27 -18 90 -16 105 2 7 8 16 15 21 15 5 0 47 23 94 50 61 37 85 56\r\n        85 71 0 11 -8 22 -17 25 -24 8 -91 34 -130 50 -17 8 -38 14 -46 14 -9 0 -18 4\r\n        -22 9 -3 5 -19 12 -36 16 -17 4 -42 13 -57 21 -15 8 -34 14 -42 14 -8 0 -24 7\r\n        -34 15 -11 8 -28 15 -37 15 -10 0 -30 6 -46 14 -15 8 -48 22 -73 31 -95 36\r\n        -126 48 -160 60 -169 61 -189 72 -198 109 l-8 31 -76 -4 c-55 -2 -83 1 -106\r\n        13 -17 9 -32 16 -34 16 -2 0 -39 -20 -83 -45z', 'fill:#f923f9', '', NULL),
(10, 'Lobby', '<p>LobbyS</p>', 'M7818 2516 c-101 -52 -182 -98 -199 -112 -8 -8 -19 -14 -22 -14 -4 0\r\n-29 -13 -55 -30 -55 -33 -78 -36 -106 -15 -29 22 -75 18 -121 -10 -22 -13 -64\r\n-36 -92 -50 -29 -14 -53 -29 -53 -33 0 -4 16 -17 35 -30 21 -13 35 -30 35 -42\r\n0 -13 14 -27 40 -40 38 -20 55 -50 27 -50 -8 0 -32 -13 -53 -28 -21 -16 -46\r\n-31 -54 -34 -8 -3 -102 -58 -208 -122 -106 -64 -196 -116 -198 -116 -3 0 -51\r\n-26 -107 -59 -56 -32 -112 -61 -125 -65 -29 -10 -27 -49 3 -54 40 -8 45 -21\r\n45 -121 0 -62 4 -101 12 -109 7 -7 32 -12 58 -12 25 0 54 -7 64 -15 11 -8 28\r\n-15 38 -15 10 0 27 -6 38 -12 16 -10 24 -10 40 0 11 6 24 12 29 12 6 0 29 13\r\n53 29 24 16 61 39 83 50 22 12 48 26 58 32 9 5 25 13 35 18 9 6 54 33 100 60\r\n46 28 90 51 98 51 8 0 17 7 20 15 4 8 10 15 14 15 5 0 43 20 84 45 42 25 83\r\n45 91 45 8 0 18 6 22 14 9 14 82 60 126 77 15 6 27 15 27 20 0 5 33 9 74 7 49\r\n-1 80 3 90 12 9 7 35 23 58 35 23 12 44 27 45 32 2 5 -15 18 -37 28 -23 10\r\n-43 22 -46 26 -9 14 9 41 35 51 25 9 25 10 8 23 -36 26 -6 63 76 96 15 6 27\r\n14 27 19 0 4 14 10 32 14 17 3 46 17 64 31 18 14 41 25 51 25 31 0 63 49 63\r\n96 0 56 8 74 36 85 32 12 30 37 -3 50 -16 6 -41 17 -56 25 -16 8 -38 14 -50\r\n14 -12 0 -31 7 -41 15 -11 8 -28 15 -38 15 -10 0 -29 6 -41 14 -12 7 -54 16\r\n-94 19 -67 5 -76 3 -135 -27z', 'fill:#5c1621', '', NULL),
(11, 'Social Garden', 'Social Garden content here ', 'M6569 2481 c-30 -15 -81 -43 -114 -61 -195 -107 -219 -118 -290 -124\r\n        -81 -8 -191 -56 -206 -89 -5 -12 -13 -29 -18 -37 -5 -8 -12 -23 -15 -33 -3 -9\r\n        -22 -27 -42 -39 l-36 -21 78 -38 c44 -20 97 -45 119 -55 22 -9 67 -29 100 -44\r\n        33 -15 78 -35 100 -45 22 -9 69 -31 105 -47 36 -16 81 -37 100 -45 19 -9 48\r\n        -23 63 -31 29 -14 94 -11 106 5 3 5 31 21 61 36 30 16 69 37 86 47 17 10 62\r\n        35 100 56 38 20 87 47 109 59 113 62 163 90 198 107 34 19 49 38 28 38 -5 0\r\n        -12 12 -16 28 -4 19 -18 33 -45 46 -32 15 -40 25 -40 46 0 27 36 70 58 70 7 0\r\n        12 9 12 20 0 11 -4 20 -8 20 -5 0 -19 9 -32 20 -13 11 -32 20 -42 20 -10 0\r\n        -29 7 -42 15 -16 11 -53 15 -127 16 -106 2 -121 5 -229 57 -30 14 -58 27 -61\r\n        28 -3 2 -30 -10 -60 -25z', 'fill:#5c1621', '', NULL),
(12, 'FMLA', 'FMLA content here ', 'M1003 2480 c-13 -5 -23 -13 -23 -17 0 -13 -62 -41 -104 -47 -22 -3\r\n        -51 -16 -65 -30 -14 -13 -59 -41 -98 -62 -75 -39 -93 -66 -53 -79 16 -5 20\r\n        -15 20 -49 0 -27 -6 -48 -15 -56 -8 -7 -15 -19 -15 -27 0 -7 7 -16 15 -19 20\r\n        -8 19 -60 -1 -68 -10 -4 -13 -13 -9 -25 4 -14 -2 -25 -24 -40 -16 -12 -36 -21\r\n        -45 -21 -22 0 -20 -30 2 -30 9 0 26 -7 36 -15 11 -8 28 -15 37 -15 12 0 19 -9\r\n        21 -27 2 -22 9 -29 33 -33 21 -4 31 -12 35 -30 4 -16 14 -26 28 -28 20 -3 42\r\n        -25 68 -68 9 -15 27 -20 84 -24 l74 -5 3 -78 c3 -85 6 -97 30 -97 9 0 24 -7\r\n        34 -16 17 -15 36 -17 134 -13 22 0 60 -8 85 -19 l44 -21 3 -67 c1 -37 6 -71\r\n        11 -76 5 -5 20 1 35 12 25 20 29 20 54 6 16 -8 36 -17 45 -20 10 -3 20 -18 24\r\n        -33 11 -52 12 -53 60 -53 36 0 54 -6 84 -30 42 -33 65 -38 74 -15 7 19 45 19\r\n        70 0 11 -8 27 -15 37 -15 9 0 20 -8 24 -18 7 -16 8 -15 15 3 16 39 11 75 -15\r\n        100 l-26 23 28 19 c15 10 71 39 123 65 470 228 551 270 560 294 5 13 14 24 20\r\n        24 16 0 12 27 -5 34 -26 10 -16 45 25 86 36 36 40 45 40 88 l0 48 -53 22 c-29\r\n        12 -61 22 -72 22 -11 0 -29 7 -39 15 -11 8 -30 15 -42 15 -12 0 -35 7 -50 15\r\n        -16 8 -43 15 -60 15 -35 0 -61 23 -66 60 -2 20 -10 26 -38 31 -19 3 -45 11\r\n        -57 17 -12 7 -31 12 -42 12 -10 0 -32 6 -48 14 -15 8 -55 22 -88 31 -63 18\r\n        -120 43 -170 75 -16 11 -39 24 -50 30 -24 12 -46 26 -98 63 -45 30 -107 37\r\n        -127 12 -13 -16 -60 -21 -60 -7 0 10 -65 42 -83 42 -13 0 -141 38 -172 51 -21\r\n        9 -174 59 -178 58 -1 0 -12 -4 -24 -9z', 'fill:#5c1621', '', NULL),
(13, 'College of Law ', 'College of Law content here ', 'M5338 1691 c-31 -15 -64 -33 -73 -41 -10 -8 -37 -12 -71 -11 -31 1\r\n        -62 -3 -69 -9 -7 -6 -48 -28 -91 -49 -44 -22 -94 -51 -113 -65 -19 -14 -40\r\n        -26 -47 -26 -6 0 -17 -6 -24 -14 -6 -8 -28 -16 -48 -18 -32 -3 -37 -6 -37 -28\r\n        0 -19 6 -25 33 -31 19 -4 39 -12 45 -18 6 -6 20 -11 32 -11 12 0 30 -7 41 -15\r\n        10 -8 27 -15 37 -15 11 0 31 -6 45 -14 43 -21 127 -46 158 -46 16 0 52 -6 79\r\n        -14 62 -16 111 -10 118 13 2 10 23 28 47 40 40 20 44 21 67 6 23 -15 26 -15\r\n        42 -1 24 22 30 20 41 -9 5 -14 22 -30 37 -35 16 -6 36 -15 45 -20 38 -22 79\r\n        -10 179 50 55 33 103 60 108 60 13 0 41 36 41 53 0 9 9 22 20 29 38 24 14 68\r\n        -38 68 -10 0 -38 14 -60 30 -23 17 -49 30 -58 30 -9 0 -28 6 -42 14 -42 22\r\n        -113 44 -165 52 -26 3 -50 10 -53 15 -3 5 -12 9 -21 9 -8 0 -29 6 -46 14 -18\r\n        8 -41 16 -52 19 -11 3 -27 7 -35 10 -8 3 -41 -6 -72 -22z', 'fill:#391345', '', NULL),
(14, 'Library', 'building content here ', 'M2445 1491 c-110 -54 -272 -133 -360 -176 l-160 -78 4 -79 c4 -87\r\n        -12 -165 -36 -183 -13 -10 -10 -15 21 -39 22 -16 36 -36 36 -49 0 -23 47 -57\r\n        80 -57 9 0 23 -7 30 -15 7 -8 19 -15 28 -15 9 0 35 -9 58 -21 33 -15 48 -18\r\n        62 -10 15 8 25 6 43 -8 29 -21 113 -46 128 -36 7 4 11 56 11 145 l0 139 -31\r\n        22 c-35 25 -33 28 41 59 30 12 84 36 119 52 67 31 117 33 141 3 6 -7 25 -17\r\n        42 -20 24 -6 55 6 174 65 l144 72 0 79 c0 67 -3 81 -17 86 -16 6 -91 59 -103\r\n        73 -3 4 -29 25 -58 48 -52 40 -56 42 -126 42 -70 0 -74 -2 -271 -99z', 'fill:#5c1621', '', NULL),
(15, 'Kennel', 'Kennel content here ', 'M6083 1559 c-28 -18 -31 -24 -26 -53 4 -25 -2 -45 -21 -79 -23 -39\r\n        -48 -57 -183 -139 -207 -123 -195 -116 -222 -128 -32 -14 -22 -22 84 -60 50\r\n        -18 104 -39 122 -46 17 -8 38 -14 46 -14 9 0 18 -4 22 -9 3 -5 19 -12 35 -16\r\n        39 -9 61 -40 53 -76 -3 -16 -2 -36 2 -46 5 -10 7 -28 6 -41 -3 -23 -2 -23 26\r\n        -8 16 8 35 22 42 30 8 9 19 16 26 16 6 0 30 13 52 29 22 16 49 32 61 35 12 4\r\n        22 12 22 17 0 5 6 9 14 9 7 0 19 7 26 15 7 8 24 15 39 15 14 1 44 12 66 25 22\r\n        14 47 28 55 32 8 3 29 17 47 30 32 25 32 26 49 165 5 38 10 47 27 50 32 4 27\r\n        22 -8 26 -16 2 -38 10 -49 18 -11 8 -29 14 -39 14 -10 0 -31 6 -45 14 -36 18\r\n        -91 37 -124 43 -22 4 -27 11 -30 44 -2 22 0 43 5 48 14 14 -1 31 -26 31 -12 0\r\n        -31 7 -41 15 -27 20 -74 17 -113 -6z', 'fill:#5c1621', '', NULL),
(16, 'School of Industrial Engineering', 'School of Industrial Engineering content here ', 'M4444 1411 c-58 -58 -94 -98 -94 -107 0 -4 7 -16 17 -26 14 -16 15\r\n        -23 3 -57 -7 -22 -10 -52 -7 -68 3 -15 2 -50 -3 -76 -6 -45 -5 -49 15 -57 13\r\n        -5 26 -14 30 -19 3 -6 21 -11 38 -11 18 0 41 -7 51 -15 11 -8 29 -15 40 -15\r\n        12 0 30 -7 40 -15 11 -8 30 -15 42 -15 13 0 25 -4 28 -9 4 -5 24 -12 45 -16\r\n        22 -4 45 -13 52 -21 6 -8 24 -14 40 -14 15 0 31 -4 34 -10 10 -16 31 -11 32 8\r\n        1 9 5 64 9 122 7 120 13 134 99 221 49 49 56 61 45 74 -7 8 -27 15 -44 15 -17\r\n        0 -40 7 -50 15 -11 8 -30 15 -43 15 -13 0 -26 7 -29 15 -4 9 -19 15 -38 15\r\n        -17 0 -40 7 -50 15 -11 8 -32 15 -47 15 -14 0 -32 7 -39 15 -8 9 -30 15 -60\r\n        15 -26 0 -63 7 -82 15 -35 14 -35 14 -74 -24z', 'fill:#f59105', '', NULL),
(17, 'Museum', 'Museum content here ', 'M3133 1310 c-23 -21 -45 -49 -49 -62 -10 -31 -16 -35 -201 -129\r\n        l-163 -82 -63 33 -63 32 -69 -34 -69 -33 2 -175 c2 -170 3 -176 26 -196 13\r\n        -12 27 -31 31 -43 3 -11 17 -24 31 -27 13 -3 24 -10 24 -14 0 -4 12 -13 28\r\n        -19 15 -6 50 -25 77 -42 28 -16 64 -37 80 -45 17 -8 44 -23 60 -34 17 -11 44\r\n        -26 60 -34 17 -8 53 -29 80 -45 28 -17 70 -41 95 -53 25 -13 57 -32 72 -42 34\r\n        -22 63 -14 77 22 6 15 15 34 19 42 19 33 33 60 89 168 32 62 64 124 71 137 13\r\n        25 81 75 102 75 9 0 115 50 150 71 8 5 22 12 30 15 29 11 85 37 100 46 17 9\r\n        19 10 74 28 30 10 43 10 61 0 37 -20 57 -6 53 37 -2 32 -7 39 -27 41 -14 2\r\n        -35 11 -47 19 -28 18 -58 10 -102 -28 -36 -32 -40 -33 -61 -20 -21 13 -6 41\r\n        29 54 36 14 40 37 6 37 -35 0 -48 21 -40 67 6 38 4 41 -31 61 -20 12 -44 22\r\n        -53 22 -9 0 -26 7 -36 15 -11 8 -27 15 -37 15 -9 0 -22 7 -29 15 -7 8 -22 15\r\n        -34 15 -11 0 -30 7 -40 15 -11 8 -30 15 -42 15 -12 0 -27 7 -34 15 -7 8 -18\r\n        15 -25 15 -7 0 -18 7 -25 15 -7 8 -20 15 -29 15 -9 0 -34 9 -56 20 -22 11 -44\r\n        20 -50 20 -6 0 -30 -18 -52 -40z', 'fill:#5c1621', '', NULL),
(18, 'College of Agriculture', 'College of Agriculture content here', 'M5012 1161 c-35 -36 -67 -75 -72 -86 -4 -11 -11 -75 -15 -142 l-7\r\n        -122 49 -22 c26 -12 55 -25 63 -28 8 -4 49 -18 90 -31 41 -13 82 -29 90 -35 8\r\n        -7 26 -15 40 -18 58 -13 116 -29 135 -38 11 -5 34 -13 50 -18 17 -5 55 -19 85\r\n        -31 30 -12 64 -24 75 -27 11 -3 34 -11 52 -19 45 -20 67 -18 115 15 24 16 59\r\n        37 78 48 93 52 99 57 106 97 6 35 3 42 -20 61 -31 25 -33 48 -6 82 15 19 17\r\n        30 10 43 -6 10 -10 31 -10 47 0 27 -5 30 -148 82 -81 29 -161 59 -179 67 -17\r\n        8 -38 14 -46 14 -9 0 -18 4 -21 9 -3 5 -21 12 -40 15 -20 4 -48 13 -63 21 -15\r\n        8 -35 15 -44 15 -9 0 -19 3 -22 6 -18 19 -122 46 -197 53 l-85 8 -63 -66z', 'fill:#014501', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fasttravel`
--

CREATE TABLE `fasttravel` (
  `ft_id` int(255) NOT NULL,
  `ft_name` varchar(255) NOT NULL,
  `ft_desc` varchar(1000) NOT NULL,
  `ft_pos` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `fasttravel`
--

INSERT INTO `fasttravel` (`ft_id`, `ft_name`, `ft_desc`, `ft_pos`) VALUES
(1, 'CBA', '', '62.64,8.03,14.75,200'),
(2, 'DAFA (Fine Arts)', '', '61.28,8.14,4.79,200'),
(3, 'CCS', '', '4.2,7.7,59.65,200'),
(4, 'SSS Hall', '', '-14.55,7.62,26.9,200'),
(5, 'CC', '', '37.73,13.43,-31.178,200'),
(6, 'CE', '', '-35.09,7.71,71.15,200'),
(7, 'Administration Rm.', '', '53.71,7.71,-32.59,200'),
(8, 'CAS', '', '33.54,7.74,-30.98,200'),
(9, 'DAFA (Architecture)', '', '1.268,11.36,-29.95,200'),
(10, 'Lobby', '', '25.299,8.25,-76.871,200'),
(11, 'Social Garden', '', '18.5,7.79,-41.24,200'),
(12, 'FMLA', '', '-62.29,7.67,41.14,200'),
(13, 'CLJ', '', '-20.7,7.78,-57.8,0'),
(14, 'Library', '', '-73.39,7.68,11.69,200'),
(15, 'Kennel', '', '-25.85,7.63,-65.54,200'),
(16, 'CIET', '', '-56.76,7.71,-45.98,200'),
(17, 'Museum', '', '-76.94,7.75,-16.11,200'),
(18, 'CA', '', '-59.21,7.63,-57.7,200');

-- --------------------------------------------------------

--
-- Table structure for table `interactables`
--

CREATE TABLE `interactables` (
  `int_id` int(255) NOT NULL COMMENT 'Interactable ID',
  `building_id` int(11) NOT NULL COMMENT 'building',
  `location` varchar(50) NOT NULL,
  `int_desc` varchar(255) NOT NULL COMMENT 'Interactable Description',
  `int_position` varchar(255) DEFAULT NULL,
  `status` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `interactables`
--

INSERT INTO `interactables` (`int_id`, `building_id`, `location`, `int_desc`, `int_position`, `status`) VALUES
(1, 1, 'CBA  office', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.sasa Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod', '54.104,7.462,24.651,160', NULL),
(2, 2, 'Fine Arts office', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.sasa Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod', '61.59,7.462,6.13,237.472', NULL),
(3, 3, 'CCS office', '<p >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo co', '6.017,7.14,62.24,201.171', NULL),
(4, 4, 'Sophia Solaire Sinco Hall', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.sasa Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod', '-8.57,7.1,26.7,244.382', NULL),
(5, 5, 'Criminology office', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.sasa Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commod', '42.146,14.37,-27.767,310.367', NULL),
(6, 6, 'CE office', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo co', '-38.07,7.1,-80.17,172.7', NULL),
(7, 7, 'Administration Room', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo co', '54.817,7.247,-36.32,313.837', NULL),
(8, 8, 'CAS office', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '33.8,7.171,-35.5,216', NULL),
(9, 9, 'Architecture office', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '-2.35,17.078,-25.91,117.707', NULL),
(10, 10, 'Lobby', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '23.31,7.645,-69.167,132.602', NULL),
(11, 11, 'Social Garden', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '19.8,7.171,-41.5,55.177', NULL),
(12, 12, 'FMLA office', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo.</p>', '-69.543,7.1,28.25,87.796', NULL),
(13, 13, 'law office', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '-23.49,7.23,-50.236,78.492', NULL),
(14, 14, 'Library', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '-75.52,7.1,6.33,70.77', NULL),
(15, 15, 'KENNEL', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.', '-25.8,7.1,-67.13,52.94', NULL),
(16, 16, 'SIET office', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '-57.97,7.1,-47.28,80.048', NULL),
(17, 17, 'Museum', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '-80.66,7.1,-20.15,56.284', NULL),
(18, 18, 'Agriculture office', '<p>\"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>', '-58.13,7.1,-57.93,66.676', NULL),
(19, 4, 'Sophia Solaire Sinco Hall', 'This is the site for the university\'s literary and musical programs, dramatic performances, and cultural displays. The hall has the latest sound system and a lighting system that only modern technology can offer.', '0.996,7.617,22.143,100.055', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Userid` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `FirstName` varchar(255) NOT NULL,
  `LastName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Userid`, `Email`, `Password`, `FirstName`, `LastName`) VALUES
(1, 'Tornare@gmail.com', '123', 'tornare', 'Fu');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alumni`
--
ALTER TABLE `alumni`
  ADD PRIMARY KEY (`alumni_count`);

--
-- Indexes for table `buildings`
--
ALTER TABLE `buildings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fasttravel`
--
ALTER TABLE `fasttravel`
  ADD PRIMARY KEY (`ft_id`);

--
-- Indexes for table `interactables`
--
ALTER TABLE `interactables`
  ADD PRIMARY KEY (`int_id`),
  ADD KEY `fk_building_id` (`building_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Userid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alumni`
--
ALTER TABLE `alumni`
  MODIFY `alumni_count` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20120023;

--
-- AUTO_INCREMENT for table `buildings`
--
ALTER TABLE `buildings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=80;

--
-- AUTO_INCREMENT for table `fasttravel`
--
ALTER TABLE `fasttravel`
  MODIFY `ft_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `interactables`
--
ALTER TABLE `interactables`
  MODIFY `int_id` int(255) NOT NULL AUTO_INCREMENT COMMENT 'Interactable ID', AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Userid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `interactables`
--
ALTER TABLE `interactables`
  ADD CONSTRAINT `fk_building_id` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
