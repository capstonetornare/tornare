$(document).ready(function () {

    const URL = 'http://localhost/Tornare/';
    let editMode = false;

    refreshData(URL + 'action/departmentview.php');

    $('#loadDestination').click(function () {
        refreshData('action/departmentview.php');
    });
   

    ///add
    $('.add-interactable').on('click', function () {
        enterAddMode();
        resetForm();
    });
    
    //Addmode
    function enterAddMode() {
        const formFields = ['Interactableid', 'Building', 'Description', 'Active', 'Style'];
    
        $('#exampleModalLabel').text('Add Interactable');
        $('button[name="save"]').prop('disabled', false);
    
        formFields.forEach(function (field) {
            var inputField = $('#' + field);
            if (field === 'Description' || field === 'Active' || field === 'Style') {
                var editor = tinymce.get(field);
                if (editor) {
                    editor.setContent('');
                    editor.mode.set('design');
                }
            } else {
                inputField.val('');
            }
            inputField.prop('disabled', false);
        });
    
        editMode = false; // Corrected to set editMode to false
    }
    
    

    ///edit
    $(document).on('click', '.edit', function () {
        let Buildingname = $(this).attr('intname1');
        let BuildingDesc = $(this).attr('intdesc1');
        let Buildingstyle = $(this).attr('intstyle1');
        let Buildingactive = $(this).attr('intact1');
        let Buildingid = $(this).attr('data-interactableid');
      

        console.log('BuildingDesc:', BuildingDesc);
        console.log('Buildingstyle:', Buildingstyle);
        console.log('Buildingactive:', Buildingactive); 
        console.log('Buildingname:', Buildingname);
        console.log('Buildingid:', Buildingid);
       

        enterEditMode(BuildingDesc, Buildingstyle, Buildingactive, Buildingname, Buildingid);
       
    });
    

///enterEditMode
function enterEditMode(BuildingDesc, Buildingstyle, Buildingactive, Buildingname, Buildingid) {
    const formFields = ['Interactableid','Building', 'Description', 'Active', 'Style'];

    $('#exampleModalLabel').text('Edit Destination');
    $('#exampleModal [name="Building"]').prop('disabled', true);

    $('button[name="save"]').prop('disabled', false);

    $("#Building").val(Buildingname || ''); // Set Buildingname or an empty string if undefined/null
    $("#Interactableid").val(Buildingid || '');

    if (tinymce.get("Description")) {
        var descriptionEditor = tinymce.get("Description");
        descriptionEditor.setContent(BuildingDesc || ''); // Set BuildingDesc or an empty string if undefined/null
        descriptionEditor.mode.set('design');
    }

    if (tinymce.get("Active")) {
        var activeEditor = tinymce.get("Active");
        activeEditor.setContent(Buildingactive || ''); // Set Buildingactive or an empty string if undefined/null
        activeEditor.mode.set('design');
    }

    if (tinymce.get("Style")) {
        var styleEditor = tinymce.get("Style");
        styleEditor.setContent(Buildingstyle || ''); // Set Buildingstyle or an empty string if undefined/null
        styleEditor.mode.set('design');
    }
    editMode = true;
    $('#exampleModal').modal('show');
}

    


    ///save
    $(document).on('click', '#save', function () {
        const inputs = $('#frmDestination input[name="Building"]');
        const description = tinymce.get("Description").getContent();
        const active = tinymce.get("Active").getContent();
        const style = tinymce.get("Style").getContent();
    
        const formData = $('#frmDestination').serialize() + "&Description=" + description + "&Active=" + active + "&Style=" + style;
    
        const emptyInputs = inputs.filter(function () {
            return $.trim($(this).val()) === '';
        });
    
        if (emptyInputs.length > 0) {
            // Handle empty inputs
            Swal.fire({
                icon: 'error',
                title: 'Error',
                text: 'Please fill out all the form fields except for Interactable ID.',
            });
            emptyInputs.first().focus();
            return;
        }
    
        let url = 'action/department_add.php';
        if (editMode) {
            url = 'action/dep_update.php';
        }
    
    
        $.ajax({
            type: 'POST',
            url: url,
            data: formData,
            dataType: 'json',
            success: function (data) {
                if (data.status) {
                refreshData(URL + 'action/departmentview.php');
                    $('button[name="save"]').prop('disabled', false);
                
                Swal.fire({
                    icon: 'success',
                    title: 'Record ' + (editMode ? 'updated' : 'added') + ' successfully',
                  }).then(() => {
                    if (!editMode) {
                        enterAddMode(); // Reset form when adding a new record
                    } else {
                        $('.dataTable tbody tr.selected').each(function () {
                            $(this).find('td:eq(1)').text(formData['Building']);
                            $(this).find('td:eq(2)').html(formData['Description']);
                            $(this).find('td:eq(3)').html(formData['Active']);
                            $(this).find('td:eq(4)').html(formData['Style']);
                        });
                    }
                });
            } else {
                if (data.message === 'Duplicate entry') {
                    Swal.fire({
                        icon: 'error',
                        title: 'Error',
                        text: 'Duplicate entry detected. Record could not be added.',
                    });
                } else {
                    Swal.fire({
                      icon: 'error',
                      title: 'Error',
                      text: 'Record could not be ' + (editMode ? 'updated' : 'added') + '. ' + data.message,
                    });
                }
            }
        },
        error: function (jqXhr, textStatus, errorMessage) {
            console.log(jqXhr.responseText);
        }
    });
});
if (editMode) { // Reset form when adding a new record
    $('.dataTable tbody tr.selected').each(function () {
        $(this).find('td:eq(1)').text(formData['Building']);
        $(this).find('td:eq(2)').html(formData['Description']);
        $(this).find('td:eq(3)').html(formData['Active']);
        $(this).find('td:eq(4)').html(formData['Style']);
    });
  }



  $(document).on('click', '.info', function () {
    let Buildingname = $(this).attr('intname1') || '';
    let BuildingDesc = $(this).attr('intdesc1') || '';
    let Buildingstyle = $(this).attr('intstyle1') || '';
    let Buildingactive = $(this).attr('intact1') || '';
    let Buildingid = $(this).attr('data-interactableid') || '';

    console.log('BuildingDesc:', BuildingDesc);
    console.log('Buildingstyle:', Buildingstyle);
    console.log('Buildingactive:', Buildingactive); 
    console.log('Buildingname:', Buildingname);
    console.log('Buildingid:', Buildingid);
   
    $('#exampleModalLabel').text('Building info');
    $('#exampleModal [name="Building"]').prop('disabled', true);
    $('button[name="save"]').prop('disabled', true);

    $("#Building").val(Buildingname);
    $("#Interactableid").val(Buildingid);

    if (tinymce.get("Description")) {
        var descriptionEditor = tinymce.get("Description");
        descriptionEditor.setContent(BuildingDesc);
        descriptionEditor.mode.set('readonly');
    }

    if (tinymce.get("Active")) {
        var activeEditor = tinymce.get("Active");
        activeEditor.setContent(Buildingactive);
        activeEditor.mode.set('readonly');
    }

    if (tinymce.get("Style")) {
        var styleEditor = tinymce.get("Style");
        styleEditor.setContent(Buildingstyle);
        styleEditor.mode.set('readonly');
    }

    $('#exampleModal').modal('show');
});



 
$(document).on('click', '.delete', function() {
    var buildingId = $(this).data('interactableid');

    Swal.fire({
        title: 'Are you sure?',
        text: 'You are about to delete this building. This action cannot be undone.',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: 'action/dep_trash.php',
                data: { buildingId: buildingId },
                    success: function(response) {
                        console.log('Building marked as trashed successfully');
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            text: 'Record has been moved to trash.',
                        }).then(() => {
                            deleteRow(); // Remove the row from the table
                            refreshData('action/departmentview.php'); // Refresh the data table after deletion
                        });
                },
                error: function(xhr, status, error) {
                    console.error('Error marking building as trashed:', error);
                }
            });
        }
    });
});



$(document).on('click', '.show-trashed', function() {
    $.ajax({
        type: 'GET',
        url: 'action/trash_building.php',
        success: function(response) {
            // Assuming response contains the HTML for trashed buildings
            $('tbody').html(response); // Update your table body with the trashed buildings
        },
        error: function(xhr, status, error) {
            console.error('Error fetching trashed buildings:', error);
        }
    });
});

function displayTrashedBuildings() {
    $.ajax({
        type: 'GET',
        url: 'action/trash_building.php',
        dataType: 'json', // Expect JSON response
        success: function(response) {
            let tableBody = $('tbody');
            tableBody.empty(); // Clear existing table data

            // Check if the response is an array and not empty
            if (Array.isArray(response) && response.length > 0) {
                response.forEach(function(building) {
                    let row = '<tr>';
                    row += '<td>' + building.id + '</td>';
                    row += '<td>' + building.name + '</td>';
                    row += '<td>' + building.description + '</td>';
                    row += '<td>' + building.map_active + '</td>';
                    row += '<td>' + building.map_style + '</td>';
                    row += '<td>';
                    row += '<div class="button-container dropdown">';
                    row += '<button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                    row += '<i class="bi bi-list" style="color: #000; font-size: 18px;"></i>';
                    row += '</button>';
                    row += '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                    row += '<button type="button" data-interactableid="' + building.id + '" class="dropdown-item retrieve">';
                    row += '<i class="bi bi-pencil-square" style="color: #4CAF50;"></i> Retrieves';
                    row += '</button>';
                    row += '<button type="button" data-interactableid="' + building.id + '" class="dropdown-item remove">';
                    row += '<i class="bi bi-trash" style="color: #F44336;"></i> Delete buildings';
                    row += '</button>';
                    row += '</div>';
                    row += '</div>';
                    row += '</td>';
                    row += '</tr>';

                    // Append the row to the table body
                    tableBody.append(row);
                });
            } else {
                // Display a message or handle empty response
                tableBody.html('<tr><td colspan="6">No trashed buildings found</td></tr>');
            }
        },
        error: function(xhr, status, error) {
            console.error('Error fetching trashed buildings:', error);
        }
    });
}

$(document).on('click', '.retrieve', function() {
    var buildingId = $(this).data('interactableid'); // Assuming the 'data-interactableid' holds the correct building ID

    Swal.fire({
        title: 'Are you sure?',
        text: 'You are about to retrieve this building',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, retrieve it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: 'action/dep_unmark.php',
                data: { buildingId: buildingId },
                success: function(response) {
                    refreshData('action/departmentview.php');   
                    console.log('Building retrieved successfully:', response);
                    // Handle success - deleteRow(), refreshData(), etc.
                },
                error: function(xhr, status, error) {
                    console.error('Error marking building as retrieved:', error);
                    // Handle error - display error message, etc.
                }
            });
        }
    });
});


$(document).on('click', '.remove', function() {
    var interactableId = $(this).data('interactableid');

    Swal.fire({
        title: 'Are you sure?',
        text: 'You are about to delete this building',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                type: 'POST',
                url: 'action/dep-delete.php', // Ang imong PHP file nga mag-handle sa deletion logic
                data: { id: interactableId }, // Pagpadala sa ID sa building nga buhaton pag-delete
                success: function(response) {
                    console.log(response);
                    refreshData('action/departmentview.php');   
                    console.log('Building deleted successfully');
                    
                    // I-check kon ang response gikan sa server kay successful
                    if (response.success === 'true') {
                        // Tawgon ang SweetAlert para ipakita ang success message
                        Swal.fire({
                            icon: 'success',
                            title: 'Success!',
                            text: 'Building deleted successfully'
                        });
                
                        // Remove ang row sa building sa table
                        $('tr[data-id="' + interactableId + '"]').remove();
                    } 
                },
                error: function(xhr, status, error) {
                    // Ayaw kalimti nga handle-on ang mga error o ipakita ang error message
                    console.error('Error deleting building:', error);
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Error deleting building: ' + error
                    });
                }
            });
        } // end if (result.isConfirmed)
    }); // end then() function
});




$(document).on('click', '.show-trashed', function() {
    displayTrashedBuildings();
});



function deleteRow() {
    var table = $('.dataTable').DataTable();
    table.row('.selected').remove().draw(false); // Remove the selected row from the table
}

        
        function resetForm() {
            editMode = false;
            $('#frmDestination')[0].reset();
        }

        function refreshData(url) {
            $('.dataTable tbody').load(url);
        }

    
        loadDepartment = function(id){
            let url = 'action/departmentview.php';
             
            $.ajax({
                  type: 'GET',
                  url: url,
                  data: {id:id},
                  success: function (data) {
                        $('.dataTable  tbody').html(data);
                  }
                });
        }

    });