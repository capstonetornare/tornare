<?php
require __DIR__ . '/../config.php';
require __DIR__ . '/../database.php';

function getInteractableItems() {
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $sql = "SELECT * FROM interactables WHERE status = 1"; // Retrieve interactables with status 1
    $result = $conn->query($sql);

    if (!$result) {
        die("Query failed: " . $conn->error);
    }

    $interactables = [];

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $interactables[] = $row;
        }
    }

    $conn->close();
    return $interactables;
}

function generateTableRow($row) {
    return "<tr data-id='" . $row["int_id"] . "'>
                <td>" . $row['int_id'] . "</td>
                <td>" . $row["location"] . "</td>
                <td>" . substr($row['int_desc'], 0, 20) . "...</td>
                <td>" . $row['int_position'] . "</td>
                <td>" . $row['building_id'] . "</td>
                <td>
                    <div class='button-container dropdown'>
                        <button class='btn btn-link' type='button' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
                            <i class='bi bi-list' style='color: #000; font-size: 18px;'></i>
                        </button>
                        <div class='dropdown-menu' aria-labelledby='dropdownMenuButton'>
                            <button intBuild1='".$row['building_id']."' intDesc1='".$row['int_desc']."' intPos1='".$row['int_position']."' intLoc1='".$row['location']."' type='button' data-interactableid='" . $row['int_id'] . "' class='dropdown-item retrieve'>
                                <i class='bi bi-pencil-square' style='color: #4CAF50;'></i> Retrieve Interactable
                            </button>
                            <button  intBuild1='".$row['building_id']."' intDesc1='".$row['int_desc']."' intPos1='".$row['int_position']."' intLoc1='".$row['location']."' type='button' data-interactableid='" . $row['int_id'] . "' class='dropdown-item remove'>
                                <i class='bi bi-info-circle' style='color: #2196F3;'></i> Delete Interactable
                            </button>
                        </div>
                    </div>
                </td>
            </tr>";
}

if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    $interactableItems = getInteractableItems(); // Retrieve interactable items with status 1

    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
        // AJAX request, return JSON
        header('Content-Type: application/json');
        echo json_encode($interactableItems);
        exit();
    } else {
        // Regular request, generate HTML
        foreach ($interactableItems as $row) {
            echo generateTableRow($row);
        }
    }
}
?>
