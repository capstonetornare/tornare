$(document).ready(function () {


  
    const URL = 'http://localhost/Tornare/';
    let editMode = false;

    refreshData(URL + 'action/view.php');
    populateDepartments(); 
    
    $('#loadDestination').click(function () {
        refreshData('action/view.php');
    });


    //Add button
    $('.add-interactable').on('click', function () {
      enterAddMode();
      resetForm();
  });

  function enterAddMode() {
    const formFields = ['Interactableid', 'Title', 'Position', 'Description', 'Department'];

    $('#exampleModalLabel').text('Add Interactable');
    $('button[name="save"]').prop('disabled', false);

    formFields.forEach(function (field) {
        var inputField = $('#' + field);
        if (field === 'Description') {
            tinymce.get(field).setContent('');
            var editor = tinymce.get(field);
            editor.mode.set('design');
        } else {
            inputField.val('');
        }
        inputField.prop('disabled', false);
    });
    editMode = false; 
    $('#exampleModal').modal('show');
   
}


    //"Edit" button
    $(document).on('click', '.edit', function () {
      let intdesc = $(this).attr('intDesc1');
      let intpost = $(this).attr('intPos1');
      let intloc = $(this).attr('intloc1');
      let intbuild = $(this).attr('intBuild1');
      let intinteract = $(this).attr('data-interactableid');

      console.log('intdesc:', intdesc);
      console.log('intbuild:', intbuild);
      console.log('intpost:', intpost); 
      console.log('intloc:', intloc);
     
      enterEditMode(intdesc, intloc, intpost, intbuild, intinteract);
    
      });

    function enterEditMode(intdesc, intloc, intpost, intbuild, intinteract) {
    const formFields = ['Interactableid','Title', 'Position', 'Description', 'Department'];

    $('#exampleModalLabel').text('Edit Destination');
    $('#exampleModal [name="Title"], [name="Department"]').prop('disabled', true);

    $('button[name="save"]').prop('disabled', false);

    $("#Interactableid").val(intinteract || ''); 
    $("#Title").val(intloc || ''); 
    $("#Position").val(intpost || ''); 
    $("#Department").val(intbuild || ''); 

    if (tinymce.get("Description")) {
        var descriptionEditor = tinymce.get("Description");
        descriptionEditor.setContent(intdesc || ''); // Set BuildingDesc or an empty string if undefined/null
        descriptionEditor.mode.set('design');
    }
    editMode = true; 
    $('#exampleModal').modal('show');
}



////save
$(document).on('click', '#save', function () {
  const inputs = $('#frmDestination input[name="Title"], #frmDestination input[name="Department"], #frmDestination input[name="Position"]')

  const emptyInputs = inputs.filter(function () {
    return $.trim($(this).val()) === '';
});

if (emptyInputs.length > 0) {
    Swal.fire({
        icon: 'error',
        title: 'Error',
        text: 'Please fill out all the form fields except for Interactable ID.',
    });

    emptyInputs.first().focus();
    return;
}

let url = 'action/add.php';
if (editMode) {
    url = 'action/update.php'; // Use the update URL for editing
}


var description = tinymce.get("Description").getContent();
        var formData = $('#frmDestination').serialize() + "&Description=" + description;

console.log (url);

  $.ajax({
    type: 'POST',
    url: url,
    data: formData,
    dataType: 'json',
    success: function (data) { 
      if (data.status) {
        refreshData(URL + 'action/view.php');
        $('button[name="save"]').prop('disabled', false);

        Swal.fire({
          icon: 'success',
          title: 'Record ' + (editMode ? 'updated' : 'added') + ' successfully',
        }).then(() => {
          if (!editMode) {
            enterAddMode();
          } else {
            $('#dataTable tbody tr.selected').each(function () {
              $(this).find('td:eq(1)').text(formData['Title']);
              $(this).find('td:eq(2)').html(formData['Description']);
              $(this).find('td:eq(3)').html(formData['Position']);
            });
          }
        });
      } else {
        if (data.message === 'Duplicate entry') {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Duplicate entry detected. Record could not be added.',
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Error',
            text: 'Record could not be ' + (editMode ? 'updated' : 'added') + '. ' + data.message,
          });
        }
      }
    },
    error: function (jqXhr, textStatus, errorMessage) {
      console.log(jqXhr.responseText);
    }
  });
});

if (editMode) {
  $('#dataTable tbody tr.selected').each(function () {
    $(this).find('td:eq(1)').text(formData['Title']);
    $(this).find('td:eq(2)').html(formData['Description']);
    $(this).find('td:eq(3)').html(formData['Position']);
  });
}

 //(Department)
    function populateDepartments() {
      $.ajax({
          type: 'GET',
          url: 'action/dep.php',
          dataType: 'json',
          success: function(response) {
              if (response.status === 1) {
                  $('#Department').empty(); // Clear any existing options
                  
                  // Create an empty optgroup element at the top
                  $('#Department').append('<optgroup label=""></optgroup>');
  
                  // Add other options within the empty optgroup
                  $('#Department optgroup').append('<option value="">Select a department</option>'); // Add a default option
                  response.departments.forEach(function(department) {
                      $('#Department optgroup').append(`<option value="${department.id}">${department.name}</option>`);
                  });
              } else {
                  console.error('Error fetching departments: ' + response.message);
                  $('#Department').html('<option value="">Error fetching departments</option>'); // Display error message
              }
          },
          error: function(xhr, status, error) {
              console.error('Error:', error);
              $('#Department').html('<option value="">Error fetching departments</option>'); // Display error message
          }
      });
  }
  
  $(document).on('click', '.info', function () {
    let intdesc = $(this).attr('intDesc1');
    let intpost = $(this).attr('intPos1');
    let intloc = $(this).attr('intloc1');
    let intbuild = $(this).attr('intBuild1');
    let intinteract = $(this).data('interactableid'); // Changed from attr() to data()
  
    console.log('intdesc:', intdesc);
    console.log('intbuild:', intbuild);
    console.log('intpost:', intpost); 
    console.log('intloc:', intloc);
  
    $('#exampleModalLabel').text('Interactable Info');  
    $('#exampleModal [name="Title"], [name="Department"], [name="Position"]').prop('disabled', true);
    $('button[name="save"]').prop('disabled', true);
  
    $("#Interactableid").val(intinteract || ''); 
    $("#Title").val(intloc || ''); 
    $("#Position").val(intpost || ''); 
    $("#Department").val(intbuild || ''); 
  
    const descriptionEditor = tinymce.get("Description");
    if (descriptionEditor) {
      descriptionEditor.setContent(intdesc || ''); // Set content or an empty string if undefined/null
      descriptionEditor.mode.set('readonly');
    }
  
    $('#exampleModal').modal('show');
  });
  
  
  $(document).on('click', '.delete', function () {
    var interactableID = $(this).data('interactableid');
    
    // Show confirmation dialog before deletion
    Swal.fire({
        title: 'Are you sure?',
        text: 'This action cannot be undone!',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            // Proceed with deletion
            $.ajax({
                method: 'POST',
                url: 'inter/trash.php',
                data: { interactableId: interactableID },
                success: function(response) {
                    // Handle success response
                    console.log(response);
                    Swal.fire({
                        icon: 'success',
                        title: 'Interactable marked as trashed!',
                        text: 'The interactable has been successfully marked as trashed.',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                    }).then(() => {
                        // Reload the page after successful deletion
                        location.reload();
                    });
                },
                error: function(xhr, status, error) {
                    // Handle different types of errors
                    console.error(error);
                    let errorMessage = "There was an error while marking the interactable as trashed. Please try again.";
                    if (xhr.status === 500) {
                        errorMessage = "Server error. Please try again later.";
                    } else if (xhr.status === 0) {
                        errorMessage = "Network error. Please check your internet connection.";
                    }
                    Swal.fire({
                        icon: 'error',
                        title: 'Error marking interactable as trashed',
                        text: errorMessage,
                        confirmButtonColor: '#d33',
                        confirmButtonText: 'OK'
                    });
                }
            });
        }
    });
});

$(document).on('click', '.show-trashed', function() {
  $.ajax({
      type: 'GET',
      url: 'inter/gettrash.php',
      success: function(response) {
          // Assuming response contains the HTML for trashed buildings
          $('tbody').html(response); // Update your table body with the trashed buildings
      },
      error: function(xhr, status, error) {
          console.error('Error fetching trashed buildings:', error);
      }
  });
});
$(document).on('click', '.show-trashed', function() {
  displayTrashedinteractables();
});

function displayTrashedinteractables() {
  $.ajax({
      type: 'GET',
      url: 'inter/gettrash.php',
      dataType: 'json', // Expect JSON response
      success: function(response) {
          let tableBody = $('tbody');
          tableBody.empty(); // Clear existing table data

          // Check if the response is an array and not empty
          if (Array.isArray(response) && response.length > 0) {
              response.forEach(function(interactable) {
                  let row = '<tr>';
                  row += '<td>' + interactable.int_id + '</td>';
                  row += '<td>' + interactable.location + '</td>';
                  row += '<td>' + interactable.int_desc + '</td>';
                  row += '<td>' + interactable.int_position + '</td>';
                  row += '<td>' + interactable.building_id + '</td>';
                  row += '<td>';
                  row += '<div class="button-container dropdown">';
                  row += '<button class="btn btn-link" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">';
                  row += '<i class="bi bi-list" style="color: #000; font-size: 18px;"></i>';
                  row += '</button>';
                  row += '<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">';
                  row += '<button type="button" data-interactableid="' + interactable.int_id + '" class="dropdown-item retrieve">';
                  row += '<i class="bi bi-pencil-square" style="color: #4CAF50;"></i> Retrieves';
                  row += '</button>';
                  row += '<button type="button" data-interactableid="' + interactable.int_id + '" class="dropdown-item remove">';
                  row += '<i class="bi bi-trash" style="color: #F44336;"></i> Delete buildings';
                  row += '</button>';
                  row += '</div>';
                  row += '</div>';
                  row += '</td>';
                  row += '</tr>';

                  // Append the row to the table body
                  tableBody.append(row);
              });
          } else {
              // Display a message or handle empty response
              tableBody.html('<tr><td colspan="6">No trashed buildings found</td></tr>');
          }
      },
      error: function(xhr, status, error) {
          console.error('Error fetching trashed buildings:', error);
      }
  });
}

$(document).on('click', '.retrieve', function() {
  var interactableId = $(this).data('interactableid'); // Assuming the 'data-interactableid' holds the correct interactable ID

  Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to retrieve this interactable',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, retrieve it!'
  }).then((result) => {
      if (result.isConfirmed) {
          $.ajax({
              type: 'POST',
              url: 'inter/unmark.php',
              data: { interactableId: interactableId }, // Update the parameter name
              success: function(response) {
                  refreshData('action/view.php'); // Assuming this refreshes interactables
                  
                  // Display Swal success notification
                  Swal.fire({
                      title: 'Interactable Retrieved',
                      text: 'The interactable has been successfully retrieved.',
                      icon: 'success',
                      confirmButtonColor: '#3085d6',
                      confirmButtonText: 'OK'
                  }).then((result) => {
                      if (result.isConfirmed) {
                          // Perform any additional actions if needed
                      }
                  });

                  console.log('Interactable retrieved successfully:', response);
                  // Handle success - deleteRow(), refreshData(), etc.
              },
              error: function(xhr, status, error) {
                  console.error('Error marking interactable as retrieved:', error);
                  // Handle error - display error message, etc.
              }
          });
      }
  });
});

$(document).on('click', '.remove', function() {
  var interactableId = $(this).data('interactableid');

  Swal.fire({
      title: 'Are you sure?',
      text: 'You are about to delete this building',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
  }).then((result) => {
      if (result.isConfirmed) {
          $.ajax({
              type: 'POST',
              url: 'inter/delete.php', // Ang imong PHP file nga mag-handle sa deletion logic
              data: { id: interactableId }, // Pagpadala sa ID sa building nga buhaton pag-delete
              success: function(response) {
                  console.log(response);
                  refreshData('action/view.php');   
                  console.log('Interactable deleted successfully');
                  
                  // I-check kon ang response gikan sa server kay successful
                  if (response.success === 'true') {
                      // Tawgon ang SweetAlert para ipakita ang success message
                      Swal.fire({
                          icon: 'success',
                          title: 'Success!',
                          text: 'Interactable deleted successfully'
                      });
              
                      // Remove ang row sa building sa table
                      $('tr[data-id="' + interactableId + '"]').remove();
                  } 
              },
              error: function(xhr, status, error) {
                  // Ayaw kalimti nga handle-on ang mga error o ipakita ang error message
                  console.error('Error deleting Interactable:', error);
                  Swal.fire({
                      icon: 'error',
                      title: 'Oops...',
                      text: 'Error deleting Interactable: ' + error
                  });
              }
          });
      } // end if (result.isConfirmed)
  }); // end then() function
});



  
    function resetForm() {
      editMode = false;
        $('#frmDestination')[0].reset();
    }

    function refreshData(url) {
      $('.dataTable tbody').load(url);
  }


  $('.paginate_button').each(function() {
    $(this).append($(this).children().get().reverse());
  });

    loadDepartment = function(id){
      
        let url = 'action/view.php';
         
        $.ajax({
          type: 'GET',
          url: url,
          data: {id:id},
          success: function (data) {
                $('#dataTable tbody').html(data);
          }
        });
      }

});

