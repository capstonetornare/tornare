$(document).ready(function() {
    // Mouse enter and leave effects
    $('.main').mouseenter(function() {
      $(this).css('opacity', '0.5');
    });
  
    $('.main').mouseleave(function() {
      $(this).css('opacity', '0');
    });
  
    // Popover function
    function showPopover(element, content, hid, name) {
      const popoverContent = $('<div class="custom-popover"><h4>'+ name +'</h4><hr>' + content + '<br><span class="popover-link interactables-link">Interactables Link</span><br><span class="popover-link department-link">Department Link</span></div>');
    
      const rect = element[0].getBoundingClientRect();
      popoverContent.css({
        position: 'absolute',
        top: rect.bottom + window.scrollY + 'px',
        left: rect.left + window.scrollX + 'px',
        background: '#333',
        color: '#fff',
        padding: '8px',
        borderRadius: '4px',
        zIndex: '9999',
        cursor: 'pointer'
      });
    
      popoverContent.find('.popover-link').css({
        textDecoration: 'underline',
        color: 'lightblue',
        cursor: 'pointer'
      });
    
      popoverContent.on('click', '.interactables-link', function(event) {
        event.stopPropagation();
        let hid = parseInt(element.attr("id"));
        window.location.href = `interactables.php?id=${hid}`;
      });
    
      popoverContent.on('click', '.department-link', function(event) {
        event.stopPropagation();
        let hid = parseInt(element.attr("id"));
        window.location.href = `department.php?id=${hid}`;
      });
    
      $('body').append(popoverContent);
    
      setTimeout(function() {
        popoverContent.remove();
      }, 2000);
    }
    
    // Click event for showing popover
    $('path.main').click(function(event) {
      event.stopPropagation(); // Prevent the click from bubbling up to higher elements
      let name = $(this).attr('name');
      let hid = $(this).attr('hid'); // Assuming hid is an attribute of the clicked element
      let content = name + " building content here";// Replace with your actual content
      showPopover($(this), content, hid, name); // Pass the name to the showPopover function
    });
    
    // Redirect to interactables.php when clicking the path directly
    $('path.main').click(function() {
      let hid = parseInt($(this).attr("id"));
      // Commented out to prevent redirection when clicking the path directly
      // window.location.href = `interactables.php?id=${hid}`;
    });
  });   