<?php
require __DIR__ . '/../config.php';
require __DIR__ . '/../database.php';

function markInteractableAsTrashed($interactableId) {
    // Connect to the database
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    // Update the status of the interactable to 'trashed'
    $stmt = $conn->prepare("UPDATE interactables SET status = 1 WHERE int_id = ?");
    $stmt->bind_param("i", $interactableId);
    $stmt->execute();
    
    if ($stmt->affected_rows > 0) {
        // Interactable marked as trashed successfully
        $stmt->close();
        $conn->close();
        echo "Interactable marked as trashed successfully"; // Echo message for debugging purposes
        return true;
    } else {
        // Failed to update
        $stmt->close();
        $conn->close();
        echo "Error marking interactable as trashed"; // Echo message for debugging purposes
        return false;
    }
}

// Check if there's a POST request from AJAX
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $interactableId = $_POST['interactableId'];

    // Call the function to update the status of the interactable
    markInteractableAsTrashed($interactableId);
}
?>
