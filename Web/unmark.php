<?php
require __DIR__ . '/../config.php';
require __DIR__ . '/../database.php';

function unmarkInteractable($interactableId) {
    $conn = new mysqli(HOST, USERNAME, PASSWORD, DATABASE);

    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }

    $stmt = $conn->prepare("UPDATE interactables SET status = 0 WHERE int_id = ?");
    $stmt->bind_param("i", $interactableId);
    $stmt->execute();

    if ($stmt->error) {
        error_log("SQL Error: " . $stmt->error);
        $stmt->close();
        $conn->close();
        echo "Error marking interactable as retrieved";
        return false;
    } elseif ($stmt->affected_rows > 0) {
        $stmt->close();
        $conn->close();
        echo "Interactable retrieved successfully";
        return true;
    } else {
        $stmt->close();
        $conn->close();
        echo "No interactable found for ID: " . $interactableId;
        return false;
    }
}

// Handle POST request to update interactable status
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_POST['interactableId'])) {
        $interactableId = $_POST['interactableId'];
        // Log or echo the received ID for debugging
        error_log("Received interactable ID: " . $interactableId);
        unmarkInteractable($interactableId); // Execute the update
    } else {
        echo "Invalid interactable ID";
    }
}
?>
